#include <iostream>

// Quick sort (three-way partition) "O (n * log n)"

// This function exchange two elements
void exchangeTwo(int *arrFirst, int *arrSecond) {
    int temp = *arrFirst;
    *arrFirst = *arrSecond;
    *arrSecond = temp;
}

/* This function partitions arr[] in three parts
   1) a[bottom...leftIterNum] contains all elements smaller than pivot
   2) a[leftIterNum+1...rightIterNum-1] contains all same to pivot elements
   3) a[rightIterNum..top] contains all elements greater than pivot */
void partition(int *arr, int bottom, int top, int& left, int& right)
{
    left = bottom - 1, right = top;
    int leftIterNum = bottom - 1, rightIterNum = top;

    while (true) {
        // From left, find the first element greater than
        // or equal to pivot. This loop will definitely
        // terminate as pivot is last element
        while (arr[++left] < arr[top]);

        // From right, find the first element smaller than
        // or equal to v
        while (arr[top] < arr[--right])
            if (right == bottom)
                break;

        // If (left) and (right) cross, then we are done
        if (left >= right)
            break;

        // Swap, so that smaller goes on left greater goes
        // on right
        exchangeTwo(&arr[left], &arr[right]);

        // Move all same left occurrence of pivot to
        // beginning of array and keep count using leftIterNum
        if (arr[left] == arr[top]) {
            ++leftIterNum;
            exchangeTwo(&arr[leftIterNum], &arr[left]);
        }

        // Move all same right occurrence of pivot to end of
        // array and keep count using rightIterNum
        if (arr[right] == arr[top]) {
            --rightIterNum;
            exchangeTwo(&arr[right], &arr[rightIterNum ]);
        }
    }

    // Move pivot element to its correct index
    exchangeTwo(&arr[left], &arr[top]);

    // Move all left same elements from beginning
    // to adjacent to arr[left]
    right = left - 1;
    for (int k = bottom; k < leftIterNum; ++k, --right)
        exchangeTwo(&arr[k], &arr[right]);

    // Move all right same elements from end
    // to adjacent to arr[left]
    ++left;
    for (int k = top - 1; k > rightIterNum ; --k, ++left)
        exchangeTwo(&arr[left], &arr[k]);
}

// Three-way partition based quick sort
void quickSort(int *arr, int bottom, int top)
{
    if (top <= bottom)
        return;

    int left, right;

    // We need to update (left and right), passed them as reference
    partition(arr, bottom, top, left, right);

    // Repeat recur for other sides
    quickSort(arr, bottom, right);
    quickSort(arr, left, top);
}

int main()
{
    int array[] = {3, 9, 2, 3, 1, 9, 4, 4, 9, 5, 1, 1, 4, 1, 24, 6, 12, 512, 0};
    int n = sizeof(array) / sizeof(int);

    std::cout << "Before Quick Sort :" << std::endl;
    for(int i = 0; i < n; ++i)
        std::cout << array[i] << " ";

    quickSort(array, 0, n - 1);

    std::cout << "\nAfter Quick Sort :" << std::endl;
    for(int i = 0; i < n; ++i)
        std::cout << array[i] << " ";

    return 0;
}
