﻿#include <iostream>
#include <ctime>
#include <conio.h>
#include <windows.h>
//*************************************************
//                    PARAMETERS                  *
//*************************************************

bool gameOver;

// map size
int width, height;

// snake's head coordinates
int snakeX, snakeY;

// fruit coordinates
int fruitX, fruitY;
int score;

// snake movement direction
enum direction {STOP, RIGHT, LEFT, UP, DOWN};
direction dir;

// snake's tail coordinates
int tailX[400], tailY[400];

// snake's tail lenght
int tailLenght;

//*************************************************

//-------------------------------------------------
//                    FUNCTIONS                   |
//-------------------------------------------------

// spawn fruit on free space of map
void spawnFruit() {

    // spawn fruit in random coordinates
    fruitX = rand() % (width - 2) + 1;
    fruitY = rand() % (height - 2) + 1;

    // if fruit's spawn coordinates equal to snake's 
    // coordinates, we should respawn fruit
    for (int i = 0; i < tailLenght; ++i) {
        if (fruitX == tailX[i] && fruitY == tailY[i])
            spawnFruit();
    }
}

// draw map, snake and fruit
void drawMap() {

    // clear screen
    Sleep(95);
    HANDLE hd = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD cd;
    cd.X = 0;
    cd.Y = 0;
    SetConsoleCursorPosition(hd, cd);

    // draw cycle
    for (int j = 0; j < height; ++j) {
        for (int k = 0; k < width; ++k) {
            if (k == 0 || k == width - 1) {
                std::cout << '#';
            } else if (k > 0 && k < width - 1 && (j == 0 || j == height - 1)) {
                std::cout << '#';
            } else if (j == snakeY && k == snakeX) {
                std::cout << 'X';
            } else if (j == fruitY && k == fruitX) {
                std::cout << 'F';
            } else {
                bool ifTail = false;
                for (int i = 0; i < tailLenght; ++i) {
                    if (tailY[i] == j && tailX[i] == k) {
                        std::cout << 'o';
                        ifTail = true;
                    }
                }
                if (!ifTail) std::cout << " ";
            }
        }
        std::cout << std::endl;
    }
    std::cout << "Score: " << score << std::endl;
}

// setup variables
void startSetup() {
    width = 20;
    height = 20;
    srand(time(NULL));
    gameOver = false;
    tailLenght = 0;
    dir = STOP;
    snakeX = width / 2;
    snakeY = height / 2;
    spawnFruit();
    score = 0;
}

// check user input
void userInput() {

    // if user pressed on keyboard
    if (_kbhit()) {

        // check this key
        switch (_getch()) {
        
        // movement direction = LEFT
        case 'a':
            // if snake move direction = RIGHT, we couldn't change it to LEFT
            if (dir == RIGHT) break;

            //else, we can
            dir = LEFT;
            break;
        
        // movement direction = RIGHT
        case 'd':
            // if snake move direction = LEFT, we couldn't change it to RIGHT
            if (dir == LEFT) break;

            //else, we can
            dir = RIGHT;
            break;

        // movement direction = UP
        case 'w':
            // if snake move direction = DOWN, we couldn't change it to UP
            if (dir == DOWN) break;

            //else, we can
            dir = UP;
            break;

        // movement direction = DOWN
        case 's':
            // if snake move direction = UP, we couldn't change it to DOWN
            if (dir == UP) break;

            //else, we can
            dir = DOWN;
            break;

        default: 
            // if user want to close the game
            gameOver = true;

        }
    }

    // if snake's head coordinates equal to fruit's coordinates:
    // spawn new fruit, increment score and snake's tail lenght
    if (snakeX == fruitX && snakeY == fruitY){
        score += 50;
        spawnFruit();
        ++tailLenght;
    }
}

// snake's game logic rules
void gameLogic() {

    // get first element from snake's tail (coordinates)
    int previousTailX = tailX[0];
    int previousTailY = tailY[0];

    // variables for next snake's tail coordinates
    int prevPreviousTailX, prevPreviousTailY;

    // set first element from snake's tail (coordinates) to snake's head coordinates
    tailX[0] = snakeX;
    tailY[0] = snakeY;

    // remember all snake's tail element coordinates
    for (int i = 1; i < tailLenght; ++i) {
        prevPreviousTailX = tailX[i];
        prevPreviousTailY = tailY[i];
        tailX[i] = previousTailX;
        tailY[i] = previousTailY;
        previousTailX = prevPreviousTailX;
        previousTailY = prevPreviousTailY;
    }

    // snake's movement
    switch (dir) {

    case LEFT:
        --snakeX;
        break;

    case RIGHT:
        ++snakeX;
        break;

    case UP:
        --snakeY;
        break;
    case DOWN:
        ++snakeY;
        break;

    }

    // if snake's head coordinates equal to border's coordinates
    // we should teleport snake on the another side of border
    if (snakeX == width - 1)
        snakeX = 1;
    else if (snakeX == 0)
        snakeX = width - 2;

    if (snakeY == height - 1)
        snakeY = 1;
    else if(snakeY == 0)
        snakeY = height - 2;

    // if snake eat her tail, then game over
    for (int i = 0; i < tailLenght; ++i) {
        if (tailX[i] == snakeX && tailY[i] == snakeY) {
            gameOver = true;
            std::cout << "GAME OVER!" << std::endl;
        }
    }
}

//-------------------------------------------------

int main()
{
    char userAnswer;
    do {
        startSetup();
        while (!gameOver) {
            drawMap();
            userInput();
            gameLogic();
        }
        std::cout << "Again (y/n)? ";
        std::cin >> userAnswer;
        system("cls");
    } while (userAnswer != 'n');

}