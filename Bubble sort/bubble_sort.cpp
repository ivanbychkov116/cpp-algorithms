#include <iostream>
#include <vector>

// bubble sort "O(n^2)"
template <class T>
void bubble_sort(std::vector<T>& a) {
    int vectorLenght = a.size();
    for(int i = 0; i < vectorLenght; ++i) {
        for(int j = 1; j < vectorLenght - i; ++j) {
            if (a[j - 1] > a[j]) {
                T temp = a[j - 1];
                a[j - 1] = a[j];
                a[j] = temp;
            }
        }
    }
}

int main() {
    std::vector<int> vec = {2, 2, 6, 123, 52, 213, 57, 90, 1, 2, 5};
    bubble_sort(vec);

    std::cout << "Sorted: ";
    for (int i = 0; i < vec.size(); ++i)
        std::cout << vec[i] << " ";
    std::cout << std::endl;
    return 0;
}