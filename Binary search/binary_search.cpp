#include <iostream>
#include <vector>

// binary search function

template <class T>
int binary_search(std::vector<T>& a, T* findValue) {
    int bottom = 0;
    int top = a.size();
    while (bottom <= top) {
        if (a[(bottom + top) / 2] == *findValue)
            return (bottom + top) / 2;
        if (a[(bottom + top) / 2] > *findValue)
            top = (bottom + top) / 2 - 1;
        else
            bottom = (bottom + top) / 2 + 1;
    }
    return -1;
}

int main() {
    std::vector<int> arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int userNum;

    std::cout << "Enter your number: ";
    std::cin >> userNum;

    if (binary_search(arr, &userNum) >= 0)
        std::cout << "Found " << userNum << " in arr[ " << binary_search(arr, &userNum) << " ]\n";
    else
        std::cout << "Not found." << std::endl;
    return 0;
}
