﻿#include <iostream>

void insertion_sort(int* arr, int arrSize)
{
	int MinimalElement;

	for (int i = 1; i < arrSize; ++i) {
		MinimalElement = arr[i];
		int PreviousIndexElement= i - 1;

		while (PreviousIndexElement >= 0 && MinimalElement < arr[PreviousIndexElement])
			arr[PreviousIndexElement + 1] = arr[PreviousIndexElement--];

		arr[PreviousIndexElement + 1] = MinimalElement;

	}
}

void selection_sort(int* arr, int arrSize) {
	int IndexMinimalElement;
	int MinimalElement;
	for (int i = 0; i < arrSize; ++i) {
		MinimalElement = arr[i];
		IndexMinimalElement = i;
		for (int j = i + 1; j < arrSize; ++j) {
			if (arr[j] < MinimalElement) {
				IndexMinimalElement = j;
				MinimalElement = arr[j];
			}
		}
		arr[IndexMinimalElement] = arr[i];
		arr[i] = MinimalElement;
	}
}

void showArray(int* arr, int arrSize) {
	for (int i = 0; i < arrSize; ++i)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}


int main() {
	int a[10] = { 3, 5, 2, 1, 10, 6, 1, 5, 6, 9 };
	showArray(a, 10);
	insertion_sort(a, 10);
	//selection_sort(a, 10);
	showArray(a, 10);

}
