﻿#include <iostream>

template <class T> 
class Stack {
    int size;
    int current;
    T* arr;

    void DataCopy(T*& arr, int& oldSize, int newSize) {

        T* temp = new T[newSize];

        for (int i = 0; i < newSize; ++i) {
            if (i < oldSize)
                temp[i] = arr[i];
            else
                temp[i] = '\0';
        }

        delete[] arr;

        oldSize = newSize;
        arr = temp;
    }

public:

    Stack(){
        size = 1;
        current = -1;
        arr = new T[size];
    }

    int GetSize() const {
        return current;
    }


    // push element and increase size of stack
    void push(const T var) {
        if ((current + 1) == size)
            DataCopy(arr, size, size * 2);
        arr[++current] = var;
    }

    // take first element and increase size of stack
    T pop() {
        T temp = arr[current];

        if ((current--) == (size / 2))
            DataCopy(arr, size, size / 2);

        return temp;
    }

    void ShowStack() const {
        for (int i = 0; i <= current; ++i)
            std::cout << arr[i] << " ";
        std::cout << std::endl;
    }
};


int main()
{
    Stack<char> newStack;

    std::cout << "Pushing in stack...\n";
    for (int i = 50; i < 80; ++i)
        newStack.push(i);

    std::cout << "Now in stack: ";
    newStack.ShowStack();

    std::cout << "\nPop from stack: ";
    for (int i = newStack.GetSize(); i >= 0; --i)
        std::cout << newStack.pop() << " ";

    return 0;
}