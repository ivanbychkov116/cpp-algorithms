#include <iostream>

// Quick sort (default) "O (n * log n)"

// This function exchange two elements
void exchangeTwo(int *arrFirst, int *arrSecond) {
    int temp = *arrFirst;
    *arrFirst = *arrSecond;
    *arrSecond = temp;
}

/* This function takes last element as pivot, places
the pivot element at its correct position in sorted
array, and places all smaller (smaller than pivot)
to left of pivot and all greater elements to right
of pivot */

int partition(int *arr, int bottom, int top) {
    int i = bottom - 1, j = top;
    while(true){
        while(arr[++i] < arr[top]);
        while(arr[--j] > arr[top]) {
            if (j == bottom)
                break;
        }
        if (i >= j)
            break;
        exchangeTwo(&arr[i], &arr[j]);
    }
    exchangeTwo(&arr[i], &arr[top]);
    return i;
}


void quickSort(int *arr, int bottom, int top) {
    if (top <= bottom) return;

    // partitioning index
    int middle = partition(arr, bottom, top);
    quickSort(arr, bottom, middle - 1);
    quickSort(arr, middle + 1, top);
}

int main() {
    int array[] = {95, 45, 48, 2325, 0, 98, 1, 485, 65, 10, 478, 1, 2325, 15, 1, 1, 2, 5, 6, 2, 1, 0};
    int n = sizeof(array)/sizeof(array[0]);

    std::cout << "Before Quick Sort :" << std::endl;
    for(int i = 0; i < n; ++i)
        std::cout << array[i] << " ";

    quickSort(array, 0, n - 1);

    std::cout << "\nAfter Quick Sort :" << std::endl;
    for(int i = 0; i < n; ++i)
        std::cout << array[i] << " ";
}

